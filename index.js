const http = require('http');
const request = require('request');
const port = process.env.PORT || 3000;
const querystring = require('querystring');
const url = 'http://netology.tomilomark.ru/api/v1/hash';
const server = http.createServer();

function parse(data, type) {
    switch (type) {
        case 'application/json':
            data = JSON.parse(data);
            break;
        case 'application/x-www-form-urlencoded':
            data = querystring.parse(data);
            break;
    }
    return data;
}

server.on('error', err => console.log(err));
server.on('request', (req, res) => {
    let data = '';
    req.on('data', chunk => data += chunk);
    req.on('end', () => {
        data = parse(data, req.headers['content-type']);
        request.post({
            url: url,
            json: true,
            method: 'POST',
            form: {
                'lastName': data['lastName']
            },
            headers: {'firstName': data['firstName']}
        }, (err, httpResponse, body) => {
            data.hash = body.hash;
            res.writeHead(200, 'OK', {'Content-Type': 'application/json'});
            res.write(JSON.stringify(data));
            res.end();
        });
    });
});

server.on('listening', () => {
    console.log('Start HTTP on port %d', port);
});
server.listen(port);
